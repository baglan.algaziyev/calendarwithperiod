package kz.mobile.calendar.calendar.model

import android.os.Parcel
import android.os.Parcelable
import kz.mobile.calendar.calendar.utils.format
import kz.mobile.calendar.calendar.utils.readDate
import kz.mobile.calendar.calendar.utils.trimTime
import kz.mobile.calendar.calendar.utils.writeDate
import java.util.*

sealed class HistoryFilter : Parcelable {
	abstract val filterName: String
	abstract val startDate: Date
	abstract val endDate: Date


	object Week : HistoryFilter() {
		override val filterName = "Week"
		override val startDate: Date get() = calendar.apply { add(Calendar.DAY_OF_MONTH, -7) }.time
		override val endDate: Date get() = Date()
	}


	object Month : HistoryFilter() {
		override val filterName = "Month"
		override val startDate: Date get() = calendar.apply { add(Calendar.MONTH, -1) }.time
		override val endDate: Date get() = Date()
	}


	object Quarter : HistoryFilter() {
		override val filterName = "Quarter"
		override val startDate: Date get() = calendar.apply { add(Calendar.MONTH, -3) }.time
		override val endDate: Date get() = Date()
	}


	object HalfYear : HistoryFilter() {
		override val filterName = "HalfYear"
		override val startDate: Date get() = calendar.apply { add(Calendar.MONTH, -6) }.time
		override val endDate: Date get() = Date()
	}


	object Year : HistoryFilter() {
		override val filterName = "Year"
		override val startDate: Date get() = calendar.apply { add(Calendar.YEAR, -1) }.time
		override val endDate: Date get() = Date()
	}


	data class Period(override val startDate: Date, override val endDate: Date) : HistoryFilter() {
		override val filterName = if (startDate.trimTime() == endDate.trimTime()) startDate.format("dd.MM.yyyy") else "${startDate.format("dd.MM.yyyy")} - ${endDate.format("dd.MM.yyyy")}"
	}


	companion object {
		@JvmField
		val CREATOR = object : Parcelable.Creator<HistoryFilter> {
			override fun newArray(size: Int): Array<out HistoryFilter?> = arrayOfNulls(size)

			override fun createFromParcel(source: Parcel?): HistoryFilter? {
				return source?.let {
					when (it.readString()) {
						Week::class.java.simpleName -> Week
						Month::class.java.simpleName -> Month
						Quarter::class.java.simpleName -> Quarter
						HalfYear::class.java.simpleName -> HalfYear
						Year::class.java.simpleName -> Year
						Period::class.java.simpleName -> Period(it.readDate(), it.readDate())
						else -> null
					}
				}
			}
		}

		private val calendar: Calendar get() = with(Calendar.getInstance()) { GregorianCalendar(get(Calendar.YEAR), get(Calendar.MONTH), get(Calendar.DAY_OF_MONTH)) }
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(javaClass.simpleName)
		if (this is Period) {
			parcel.writeDate(startDate)
			parcel.writeDate(endDate)
		}
	}

	override fun describeContents() = 0
}