package kz.mobile.calendar.calendar.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import kz.mobile.calendar.R;
import kz.mobile.calendar.calendar.model.CalendarListener;
import kz.mobile.calendar.calendar.utils.Dates;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


public class CalendarFragment extends Fragment {

	private CalendarListener listener;
	private final DisplayMetrics metrics = new DisplayMetrics();
	private Calendar first;
	private Calendar second;


	public static CalendarFragment newInstance(CalendarListener listener) {
		CalendarFragment fragment = new CalendarFragment();
		fragment.listener = listener;
		return fragment;
	}


	public int getMonthInterval() {
		if (first.get(Calendar.YEAR) == second.get(Calendar.YEAR)) {
			return first.get(Calendar.MONTH) - second.get(Calendar.MONTH) + 1;
		} else {
			return (first.get(Calendar.YEAR) - second.get(Calendar.YEAR)) * 12 + first.get(Calendar.MONTH) - second.get(Calendar.MONTH) + 1;
		}
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.calendar_fragment, container, false);

		first = Calendar.getInstance();
		first.setTime(new Date());

		second = Calendar.getInstance();
		second.set(Calendar.YEAR, 2010);
		second.set(Calendar.MONTH, 0);
		second.set(Calendar.DAY_OF_MONTH, 1);

		final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.calendar_recycler_view);
		final LinearLayoutManager layout = new LinearLayoutManager(getContext());
		layout.setStackFromEnd(true);
		recyclerView.setLayoutManager(layout);

		final GridAdapter adapter = new GridAdapter(getContext(), getMonthInterval(), new AdapterUpdateDelegate() {
			@Override
			public void postAndNotifyAdapter(final RecyclerView.Adapter adapter) {
				recyclerView.post(new Runnable() {
					@Override
					public void run() {
						if (recyclerView.isComputingLayout()) {
							postAndNotifyAdapter(adapter);
						} else {
							adapter.notifyDataSetChanged();
						}
					}
				});
			}
		});
		recyclerView.setAdapter(adapter);

		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return view;
	}


	private class MonthAdapter extends BaseAdapter {
		private GregorianCalendar month;
		private int offset;
		private int count;
		private int selectionFirstPosition;
		private int selectionStartPosition;
		private int selectionEndPosition;
		private int selectionLastPosition;
		private int disabledToPosition;
		private int disabledEndPosition;
		private boolean isSelectedMonth = false;

		private Context context;


		MonthAdapter(Context context) {
			this.context = context;
		}


		void prepare(GregorianCalendar month, GregorianCalendar selectionStartDate, GregorianCalendar selectionEndDate, boolean isSelectedMonth) {
			this.month = month;
			this.isSelectedMonth = isSelectedMonth;

			offset = month.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY;
			if (offset < 0) {
				offset += 7;
			}

			count = month.getActualMaximum(Calendar.DAY_OF_MONTH);

			if (!isSelectedMonth) {
				disabledToPosition = selectionFirstPosition = selectionStartPosition = selectionEndPosition = selectionLastPosition = disabledEndPosition = -1;
				if (selectionStartDate != null) {
					if (selectionEndDate == null) {
						if (month.get(Calendar.YEAR) == selectionStartDate.get(Calendar.YEAR) && month.get(Calendar.MONTH) == selectionStartDate.get(Calendar.MONTH)) {
							selectionFirstPosition = selectionStartDate.get(Calendar.DAY_OF_MONTH) + offset - 1;
						}
					} else if (selectionEndDate.after(month) || selectionEndDate.equals(month)) {
						final GregorianCalendar monthEnd = (GregorianCalendar) month.clone();
						monthEnd.add(Calendar.MONTH, 1);
						monthEnd.add(Calendar.DAY_OF_MONTH, -1);

						if (selectionStartDate.before(monthEnd) || selectionStartDate.equals(monthEnd)) {
							if (selectionStartDate.before(month)) {
								selectionStartPosition = offset;
							} else {
								selectionFirstPosition = selectionStartDate.get(Calendar.DAY_OF_MONTH) + offset - 1;
								selectionStartPosition = selectionStartDate.get(Calendar.DAY_OF_MONTH) + offset;
							}

							if (selectionEndDate.after(monthEnd)) {
								selectionEndPosition = offset + count;
							} else {
								selectionEndPosition = selectionEndDate.get(Calendar.DAY_OF_MONTH) + offset - 2;
								selectionLastPosition = selectionEndDate.get(Calendar.DAY_OF_MONTH) + offset - 1;
							}
						}
					}
				}
			}

			if (second != null && month.get(Calendar.YEAR) == second.get(Calendar.YEAR) && month.get(Calendar.MONTH) == second.get(Calendar.MONTH)) {
				disabledToPosition = second.get(Calendar.DAY_OF_MONTH) + offset - 1;
			}

			if (first != null && month.get(Calendar.YEAR) == first.get(Calendar.YEAR) && month.get(Calendar.MONTH) == first.get(Calendar.MONTH)) {
				disabledEndPosition = first.get(Calendar.DAY_OF_MONTH) + offset;
			}
		}


		@Override
		public int getCount() {
			return offset + count;
		}


		@Override
		public GregorianCalendar getItem(int position) {
			final int dayOfMonth = position - offset + 1;
			if (dayOfMonth > 0) {
				month.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				return month;
			} else {
				return null;
			}
		}


		@Override
		public long getItemId(int position) {
			return 0;
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final TextView textView = convertView != null ? (TextView) convertView : new TextView(parent.getContext());

			textView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorForeground));
			textView.setGravity(Gravity.CENTER);
			textView.setWidth(metrics.widthPixels / 7);
			textView.setHeight(metrics.widthPixels / 7);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
			textView.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimary));
			final int dayOfMonth = position - offset + 1;
			if (dayOfMonth > 0) {
				textView.setText(String.valueOf(dayOfMonth));
				if (isSelectedMonth) {
					textView.setBackgroundColor(ContextCompat.getColor(context, R.color.windowBackground));
				} else {
					if (position == selectionFirstPosition && selectionEndPosition == -1) {
						textView.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimaryInverse));
						textView.setBackgroundResource(R.drawable.oval);
					} else if (position == selectionFirstPosition) {
						textView.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorPrimaryInverse));
						textView.setBackgroundResource(R.drawable.oval_start);
					} else if (position == selectionLastPosition) {
						textView.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorPrimaryInverse));
						textView.setBackgroundResource(R.drawable.oval_end);
					} else if (position >= selectionStartPosition && position <= selectionEndPosition) {
						textView.setBackgroundColor(ContextCompat.getColor(context, R.color.windowBackground));
					}
				}
				if (position >= disabledEndPosition && disabledEndPosition != -1) {
					textView.setTextColor(ContextCompat.getColor(context, R.color.textColorHint));
				}
				if (position <= disabledToPosition && disabledToPosition != -1) {
					textView.setTextColor(ContextCompat.getColor(context, R.color.textColorHint));
				}
			} else {
				textView.setText("");
			}
			return textView;
		}

	}


	private class ViewHolder extends RecyclerView.ViewHolder {

		private final TextView title;
		private final GridView grid;
		private Context context;


		public ViewHolder(View itemView, AdapterView.OnItemClickListener gridClickListener, View.OnClickListener monthClickListener, Context context) {
			super(itemView);
			this.context = context;
			title = (TextView) itemView.findViewById(R.id.month_title);
			title.setOnClickListener(monthClickListener);
			grid = (GridView) itemView.findViewById(R.id.month_grid);
			grid.setAdapter(new MonthAdapter(context));
			grid.setOnItemClickListener(gridClickListener);
		}


		void prepare(GregorianCalendar month, GregorianCalendar selectionStartDate, GregorianCalendar selectionEndDate, GregorianCalendar lastSelectedMonth) {
			boolean isSelectedMonth;
			Date currentDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentDate);
			if (Dates.isCurrentYear(month.getTime())) {
				title.setText(Dates.format(month.getTime(), "LLLL"));
			} else {
				title.setText(Dates.format(month.getTime(), "LLLL ’yy"));
			}
			if (lastSelectedMonth != null
					&& lastSelectedMonth.get(Calendar.MONTH) == month.get(Calendar.MONTH)
					&& month.get(Calendar.YEAR) == lastSelectedMonth.get(Calendar.YEAR)) {
				title.setBackgroundColor(ContextCompat.getColor(context, R.color.custom_red));
				title.setTextColor(ContextCompat.getColor(getContext(), R.color.textColorPrimaryInverse));
				isSelectedMonth = true;
			} else {
				title.setBackgroundColor(ContextCompat.getColor(context, R.color.white_2));
				title.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimary));
				isSelectedMonth = false;
			}
			((MonthAdapter) grid.getAdapter()).prepare(month, selectionStartDate, selectionEndDate, isSelectedMonth);
			grid.forceLayout();
		}
	}


	private class GridAdapter extends RecyclerView.Adapter<ViewHolder> {
		private GregorianCalendar startDate;
		private GregorianCalendar endDate;
		private GregorianCalendar lastSelectedMonth;
		private final Context context;
		private final int monthCount;
		private final AdapterUpdateDelegate updateDelegate;


		public GridAdapter(Context context, int monthCount, AdapterUpdateDelegate updateDelegate) {
			this.context = context;
			this.monthCount = monthCount;
			this.updateDelegate = updateDelegate;
		}


		private final AdapterView.OnItemClickListener gridClickListener = new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				lastSelectedMonth = null;
				final GregorianCalendar date = (GregorianCalendar) parent.getItemAtPosition(position);
				if (date != null && date.getTime().getTime() > second.getTime().getTime() && date.getTime().getTime() < first.getTime().getTime()) {
					if (startDate == null && endDate == null) {
						startDate = date;
						listener.dateSelected(date);
					} else if (startDate != null && endDate != null) {
						startDate = date;
						endDate = null;
						listener.dateSelected(date);
					} else {
						if (date.equals(startDate)) {
							startDate = null;
						} else if (startDate.before(date)) {
							endDate = date;
						} else {
							endDate = startDate;
							startDate = date;
						}
						listener.periodSelected(startDate, endDate);
					}
					updateDelegate.postAndNotifyAdapter(GridAdapter.this);
				}
			}
		};

		private final View.OnClickListener monthClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Integer position = (Integer) v.getTag();
				final GregorianCalendar month = new GregorianCalendar();
				month.add(Calendar.MONTH, position - getItemCount() + 1);
				if (lastSelectedMonth != null && lastSelectedMonth.get(Calendar.MONTH) == month.get(Calendar.MONTH)) {
					lastSelectedMonth = null;
					startDate = null;
					endDate = null;
					listener.monthSelected(null, null);
				} else {
					lastSelectedMonth = month;
					month.set(Calendar.DAY_OF_MONTH, month.getActualMaximum(Calendar.DAY_OF_MONTH));
					endDate = (GregorianCalendar) month.clone();
					month.set(Calendar.DAY_OF_MONTH, 1);
					startDate = (GregorianCalendar) month.clone();
					listener.monthSelected(startDate, endDate);
				}
				notifyDataSetChanged();
			}
		};


		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			final View monthLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_month_layout, parent, false);
			return new ViewHolder(monthLayout, gridClickListener, monthClickListener, context);
		}


		@Override
		public void onBindViewHolder(ViewHolder holder, int position) {

			final GregorianCalendar month = new GregorianCalendar();
			month.set(Calendar.DAY_OF_MONTH, 1);
			month.add(Calendar.MONTH, position - getItemCount() + 1);
			month.set(Calendar.HOUR_OF_DAY, 0);
			month.set(Calendar.MINUTE, 0);
			month.set(Calendar.SECOND, 0);
			month.set(Calendar.MILLISECOND, 0);

			month.setTimeZone(TimeZone.getDefault());
			holder.prepare(month, startDate, endDate, lastSelectedMonth);
			holder.title.setTag(position);
		}

		@Override
		public int getItemCount() {
			return monthCount;
		}
	}


	private interface AdapterUpdateDelegate {
		void postAndNotifyAdapter(RecyclerView.Adapter adapter);
	}
}