package kz.mobile.calendar.calendar.views

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kz.mobile.calendar.MainActivity
import kz.mobile.calendar.R
import kz.mobile.calendar.calendar.model.CalendarListener
import kz.mobile.calendar.calendar.model.HistoryFilter
import kz.mobile.calendar.calendar.utils.checkNotNull2
import kz.mobile.calendar.calendar.utils.format
import kz.mobile.calendar.calendar.utils.isCurrentYear
import kz.mobile.calendar.databinding.DialogCalendarViewActivityBinding

import java.util.*


class PeriodFilterDialog : AppCompatDialogFragment() {
	private lateinit var bindings: DialogCalendarViewActivityBinding
	private var startGregorianDate: GregorianCalendar? = null
	private var endGregorianDate: GregorianCalendar? = null
	private var selectedGregorianDate: GregorianCalendar? = null

	private val calendarListener = object : CalendarListener {
		override fun periodSelected(dateFrom: GregorianCalendar?, dateTo: GregorianCalendar?) {
			with(bindings) {
				startGregorianDate = dateFrom
				endGregorianDate = dateTo
				selectedGregorianDate = null
				if (dateFrom != null && dateTo != null) {
					selectedPeriod.visibility = View.VISIBLE
					selectedDate.visibility = View.GONE
					startDate.setText(formatDate(dateFrom.time))
					endDate.setText(formatDate(dateTo.time))
				} else if (dateFrom == null && dateTo == null) {
					selectedPeriod.visibility = View.GONE
					selectedDate.visibility = View.VISIBLE
					selectedDate.text = getString(R.string.select_period)
				}
			}
		}

		override fun dateSelected(date: GregorianCalendar?) {
			with(bindings) {
				selectedGregorianDate = null
				startGregorianDate = null
				endGregorianDate = null
				if (date != null) {
					val calendar = Calendar.getInstance()
					calendar.time = Date()
					selectedGregorianDate = date
					selectedPeriod.visibility = View.GONE
					selectedDate.visibility = View.VISIBLE
					selectedDate.text = formatDate(date.time)
				}
			}
		}

		override fun monthSelected(dateFrom: GregorianCalendar?, dateTo: GregorianCalendar?) {
			with(bindings) {
				startGregorianDate = dateFrom
				endGregorianDate = dateTo
				selectedGregorianDate = null
				if (dateFrom != null && dateTo != null) {
					selectedPeriod.visibility = View.GONE
					selectedDate.visibility = View.VISIBLE
					selectedDate.text = startGregorianDate?.time?.let { formatMonth(it) }
				} else if (dateFrom == null && dateTo == null) {
					selectedPeriod.visibility = View.GONE
					selectedDate.visibility = View.VISIBLE
					selectedDate.text = getString(R.string.select_period)
				}
			}
		}
	}

	override fun onCreate(state: Bundle?) {
		super.onCreate(state)
		setStyle(DialogFragment.STYLE_NORMAL, R.style.Fullscreen)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, state: Bundle?): View? {
		super.onCreateView(inflater, container, state)
		bindings = DialogCalendarViewActivityBinding.inflate(inflater, container, false)
		bindings.closeCalendarButton.setOnClickListener { dismiss()}
		openCalendarFragment()
		bindings.filterActionButton.setOnClickListener {
			if (startGregorianDate != null && endGregorianDate != null) {
				startGregorianDate?.set(Calendar.HOUR_OF_DAY, 0)
				startGregorianDate?.set(Calendar.MINUTE, 0)
				startGregorianDate?.set(Calendar.SECOND, 0)
				startGregorianDate?.set(Calendar.MILLISECOND, 0)
				endGregorianDate?.set(Calendar.HOUR_OF_DAY, 23)
				endGregorianDate?.set(Calendar.MINUTE, 59)
				endGregorianDate?.set(Calendar.SECOND, 59)
				endGregorianDate?.set(Calendar.MILLISECOND, 0)
				checkNotNull2(startGregorianDate?.time, endGregorianDate?.time)?.let { (startDate, endDate) ->
					(activity as MainActivity).setDate(HistoryFilter.Period(startDate, endDate))
				}
			} else if (selectedGregorianDate != null) {
				val month = selectedGregorianDate?.clone() as GregorianCalendar
				month.set(Calendar.HOUR_OF_DAY, 23)
				month.set(Calendar.MINUTE, 59)
				month.set(Calendar.SECOND, 59)
				month.set(Calendar.MILLISECOND, 0)
				selectedGregorianDate?.time?.let { selectedDate -> (activity as MainActivity).setDate(HistoryFilter.Period(selectedDate, month.time)) }
			} else {
				Toast.makeText(context, "Необходимо выбрать период" , Toast.LENGTH_LONG).show()
			}
		}
		return bindings.root
	}

	private fun formatDate(date: Date) = if (date.isCurrentYear) date.format("d MMMM") else date.format("d MMMM ’yy")

	private fun formatMonth(date: Date) = if (date.isCurrentYear) date.format("LLLL") else date.format("LLLL ’yy")


	private fun openCalendarFragment() {
		val transaction = childFragmentManager.beginTransaction()
		transaction.replace(bindings.calendarContainer.id, CalendarFragment.newInstance(calendarListener), "calendarFragment")
		transaction.commit()
	}
}