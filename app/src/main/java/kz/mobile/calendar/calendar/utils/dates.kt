@file:JvmName("Dates")

package kz.mobile.calendar.calendar.utils

import android.annotation.SuppressLint
import android.os.Parcel
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.format(format: String): String = SimpleDateFormat(format).format(this)

@SuppressLint("SimpleDateFormat")
fun String.parse(format: String): Date = SimpleDateFormat(format).parse(this)

fun Date.formatFlex(): String = when {
	isCurrentYear && before(previousMonth) -> format("LLLL")
	isCurrentYear -> format("d MMMM")
	before(previousMonth) -> format("LLLL yyyy")
	else -> format("d MMMM ’yy")
}

fun Long.timeFormatFlex(): String {
	val date = Date(this)
	return when {
		date.isCurrentYear && date.before(previousMonth) -> date.format("LLLL")
		date.isCurrentYear -> date.format("d MMMM")
		date.before(previousMonth) -> date.format("LLLL yyyy")
		else -> date.format("d MMMM ’yy")
	}
}


fun Date.trimTime(): Date = with(calendar) { GregorianCalendar(get(Calendar.YEAR), get(Calendar.MONTH), get(Calendar.DAY_OF_MONTH)).time }

fun Date.trimDay(): Date = with(calendar) { GregorianCalendar(get(Calendar.YEAR), get(Calendar.MONTH), 1).time }

val Date.isCurrentYear
	get() = calendar[Calendar.YEAR] == Calendar.getInstance()[Calendar.YEAR]

val previousMonth: Date
	get() {
		val cal = Calendar.getInstance().apply { add(Calendar.MONTH, -1) }
		return GregorianCalendar(cal[Calendar.YEAR], cal[Calendar.MONTH], 1).time
	}

val Date.isToday
	get() = isCurrentYear && calendar[Calendar.DAY_OF_YEAR] == Calendar.getInstance()[Calendar.DAY_OF_YEAR]

val Date.isYesterday
	get() = isCurrentYear && calendar[Calendar.DAY_OF_YEAR] == Calendar.getInstance()[Calendar.DAY_OF_YEAR] - 1

private val Date.calendar get() = Calendar.getInstance().also { it.time = this }



inline fun Parcel.readDate(): Date = readLong().let { if (it == -1L) throw IllegalStateException("Expected Date, got null") else Date(it) }

inline fun Parcel.writeDate(value: Date?): Unit = if (value == null) writeLong(-1) else writeLong(value.time)