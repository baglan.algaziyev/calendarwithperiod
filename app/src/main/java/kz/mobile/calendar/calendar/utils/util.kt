package kz.mobile.calendar.calendar.utils

fun <T, R> checkNotNull2(first: T?, second: R?): Pair<T, R>? = if (first == null || second == null) null else Pair(first, second)