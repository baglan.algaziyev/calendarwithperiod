package kz.mobile.calendar.calendar.model

import java.util.GregorianCalendar


interface CalendarListener {
	fun periodSelected(startDate: GregorianCalendar?, endDate: GregorianCalendar?)
	fun dateSelected(selectedDate: GregorianCalendar?)
	fun monthSelected(startDate: GregorianCalendar?, endDate: GregorianCalendar?)
}
