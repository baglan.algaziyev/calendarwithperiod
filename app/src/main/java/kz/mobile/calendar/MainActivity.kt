package kz.mobile.calendar

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AppCompatDialog
import android.support.v7.app.AppCompatDialogFragment
import android.widget.Button
import android.widget.TextView
import kz.mobile.calendar.calendar.model.HistoryFilter
import kz.mobile.calendar.calendar.utils.format
import kz.mobile.calendar.calendar.views.PeriodFilterDialog
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.cal).setOnClickListener {
            pushDialog(PeriodFilterDialog())
        }
    }


    fun setDate(period: HistoryFilter.Period) {
        popFragment()
        val date = findViewById<TextView>(R.id.text)
        date.text = "${period.startDate.format("dd.MM.yyyy")} - ${period.endDate.format("dd.MM.yyyy")} "
    }


    private fun pushDialog(dialog: AppCompatDialogFragment) {
        val name = UUID.randomUUID().toString()
        supportFragmentManager
                .beginTransaction()
                .addToBackStack(name)
                .add(dialog, name)
                .commit()
    }

    private fun popFragment() {
        supportFragmentManager.popBackStackImmediate()
    }
}
